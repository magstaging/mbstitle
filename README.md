clone the repository

create folder app/code/Mbs/Title when located at the root of the Magento site

copy the content of this repository within the folder

install the module php bin/magento setup:upgrade

add a product attribute called 'brand' and go to a product page,

verify the title for the product is prepended with the product brand if the brand value has a value for this product
